<h4>Nova senha {{env("APP_NAME")}}:</h4>
<hr>
<h5>{{$newPassword}}</h5>
<hr>
<p>Guarde sua senha, ou altere a mesma no {{env("APP_NAME")}}.</p>