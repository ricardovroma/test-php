<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello Bulma!</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<section class="section">
    <div class="container">
        <h1 class="title">
            Hi, welcome to this awesome url shortener
        </h1>
        <form action="{{ route('home') }}" method="POST">
            {{ csrf_field() }}
            <div class="field">
                <label class="label">URL</label>
                <div class="control">
                    <input class="input" name="url" type="url" placeholder="Url">
                    <div id="errors"></div>
                </div>
            </div>

            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-link" name="send">Submit</button>
                </div>
                <div class="control">
                    <button class="button is-link is-light">Cancel</button>
                </div>
            </div>
        </form>

        <div class="field is-grouped shorten_url">
            @if($data['url_resource'])
            <h2>Your shorten url is: </h2>
            <div><h3><a href="{{ $data['url_resource']['shorten_url'] }}">{{ $data['url_resource']['shorten_url'] }}</a></h3></div>
            @endif
        </div>

        <h1 class="title" style="margin-top: 100px;">
            Top 100
        </h1>
        @foreach($data['urls'] as $url)
        <div class="field">
            <label class="label">({{ $url->accessed }}) @if($url->title){{ $url->title }}@else Carregando.. @endif</label>
            <div class="control">
                <a href="{{ $url->url }}">{{ $url->url }}</a>
            </div>
        </div>
        @endforeach
    </div>
</section>
<script>

</script>
</body>
</html>