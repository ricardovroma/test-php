<?php

namespace App;

use App\Mail\NewPasswordMail;
use App\Models\Url;
use Exception;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param string $email
     * @param string $newPassword
     * @throws Exception
     */
    public static function sendNewPasswordEmail(string $email, string $newPassword)
    {
        try
        {
            Mail::to($email)->send(new NewPasswordMail($newPassword));
        } catch (Exception $exception)
        {
            throw new Exception($exception->getMessage());
        }
    }
}
