<?php


namespace App\Utils;

use Closure;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Validator;
use ReflectionClass;

class FieldsTranslation
{
    /**
     * @return Collection
     */
    public static function getAllModels()
    {
        $models = collect(File::allFiles(app_path()))
            ->map(function ($item) {
                $path = $item->getRelativePathName();
                $class = sprintf('\%s%s', Container::getInstance()->getNamespace(),
                    strtr(substr($path, 0, strrpos($path, '.')), '/', '\\'));

                return $class;
            })
            ->filter(function ($class) {
                $valid = false;

                if (class_exists($class)) {
                    $reflection = new ReflectionClass($class);
                    $valid = $reflection->isSubclassOf(Model::class);
                }

                return $valid;
            });

        return $models->values();
    }

    /**
     * @param string $model
     * @return array|mixed
     */
    public static function getModelTranslateField(string $model)
    {
        $modelInstance = new $model();

        if (!method_exists($modelInstance, 'translateFields'))
        {
            return [];
        }

        return $modelInstance->translateFields();
    }

    /**
     * @return array
     */
    public static function getAllTranslatedFields()
    {
        $models = self::getAllModels();
        $translatedFields = [];

        foreach ($models as $model)
        {
            $keys = array_keys(self::getModelTranslateField($model));
            $translatedKeys = [];

            foreach ($keys as $key)
            {
                $translatedKeys[] = $key;
            }

            foreach ($translatedKeys as $field)
            {
                $translatedFields += [$field => self::getModelTranslateField($model)[$field]];
            }
        }
        return $translatedFields;
    }

    /**
     * @param $validator
     * @param $class
     * @return Validator
     */
    public static function myTranslation($validator, string $class): Validator
    {
        $validatorKeys = $validator->errors()->keys();
        $translatedModelKeys = null;

        if (method_exists($class, 'translateFields'))
        {
            $translatedModelKeys = collect($class::translateFields());
        }

        $attributesNames = [];
        foreach ($validatorKeys as $validatorKey)
        {
            if (!is_null($translatedModelKeys))
            {
                if (!!$validator->errors()->has($validatorKey) and !$translatedModelKeys->has($validatorKey))
                {
                    $attributesNames[$validatorKey] = $validatorKey;
                }
            }
            else
            {
                $attributesNames[$validatorKey] = $validatorKey;
            }
        }
        return (clone $validator)->setAttributeNames(array_flip($attributesNames));
    }

    /**
     * @param $validator
     * @param Closure $closure
     * @return Validator
     */
    public static function closureTranslation($validator, Closure $closure) : Validator
    {
        $closureAttributes = $closure();
        $closureAttributesKeys = collect($closureAttributes)->keys()->flip();
        $validatorKeys = collect($validator->errors());
        $attributesNames = [];

        foreach ($validatorKeys as $validatorKey => $validatorValue)
        {
            if ($closureAttributesKeys->has($validatorKey))
            {
                $attributesNames[$validatorKey] = $closureAttributes[$validatorKey];
            }
            else
            {
                $attributesNames[$validatorKey] = $validatorKey;
            }
        }

        return (clone $validator)->setAttributeNames($attributesNames);
    }
}
