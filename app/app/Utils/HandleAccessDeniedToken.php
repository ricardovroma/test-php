<?php


namespace App\Utils;

use League\OAuth2\Server\Exception\OAuthServerException;
use Throwable;

class HandleAccessDeniedTokenException
{
    /**
     * @param Throwable $exception
     * @return bool
     */
    public static function isAccessDeniedTokenException(Throwable $exception)
    {
        return $exception instanceof OAuthServerException and $exception->getCode() == 9 ? true : false;
    }
}
