<?php

namespace App\Utils;

use Closure;
use Illuminate\Support\Facades\Validator;

class MyValidator extends Validator
{
    /**
     * @param array $data
     * @param array $rules
     * @param $methodClass
     * @param array $messages
     * @param array $customAttributes
     * @return \Illuminate\Validation\Validator
     */
    public static function myMake(array $data, array $rules, $methodClass, $messages = [], $customAttributes = [])
        : \Illuminate\Validation\Validator
    {
        $validator = parent::make($data, $rules, $messages, $customAttributes);

        if ($methodClass instanceof Closure)
        {
            return FieldsTranslation::closureTranslation($validator, $methodClass);
        }

        return FieldsTranslation::myTranslation($validator, $methodClass);
    }
}
