<?php

namespace App\Models;

use App\Jobs\UrlShortJob;
use App\Service\UrlService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $table = 'url';
    protected $fillable = ['url', 'shorten_url', 'accessed', 'title'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function save(array $options = [])
    {
        $_return = parent::save($options);

        if(!$this->title) {
            UrlShortJob::dispatch($this);
        }
        return $_return;
    }

    public function get_shorten_url(): string
    {
        return env('APP_URL', false)."/see/".$this->shorten_url;
    }
}
