<?php

namespace App\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;

class NewPasswordRule implements Rule
{
    public $value;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;

        if ($this->emailExists($value) and $this->isActiveUser($value))
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (!$this->emailExists($this->value))
        {
            return 'Email inválido';
        }

        if (!$this->isActiveUser($this->value))
        {
            return 'Usuário inativo';
        }
    }

    /**
     * @param string $value
     * @return bool
     */
    public function emailExists(string $value) : bool
    {
        return User::where('email', $value)->exists();
    }

    /**
     * @param string $value
     * @return bool
     */
    public function isActiveUser(string $value) : bool
    {
        return User::where('email', $value)->where('active', true)->exists();
    }
}
