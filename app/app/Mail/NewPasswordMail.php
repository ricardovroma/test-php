<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewPasswordMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var string
     */
    public $newPassword;

    /**
     * Create a new message instance.
     *
     * @param string $newPassword
     */
    public function __construct(string $newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fields = ['newPassword' => $this->newPassword];

        return $this->view('user.new_password', $fields)->subject(env('APP_NAME') . " - Nova senha");
    }
}
