<?php
namespace App\Service;

use App\Models\Url;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Http;

class UrlService {

    /**
     * @param String $shorten_url
     * @return Url
     * @throws ModelNotFoundException
     */
    public static function get_shorten_url(String $shorten_url): Url
    {
        $url = Url::where('shorten_url', $shorten_url)->firstOrFail();
        $url->accessed = (int)$url->accessed + 1;
        $url->save();
        return $url;
    }

    public static function get_url_metadata(String $url): array
    {
        $title = "Not found";
        try {
            $response = Http::get($url);
            if (preg_match('/<title>(.+)<\/title>/', $response,$matches) && isset($matches[1])) {
                $title = $matches[1];
            }
        }catch (\Exception $e) {}

        return [
            'title' => $title,
        ];
    }

    public static function shortIt(String $url, int $id): string {
        return uniqid().$id;
    }

    public static function save($url_path): Url
    {
        $url = Url::where("url", $url_path)->first();
        if($url == null) {
            $url = Url::create([
                'url' => $url_path,
                'shorten_url' => $url_path,
                'accessed' => 0,
            ]);
            $shorten_url = UrlService::shortIt($url, $url->id);
            $url->shorten_url = $shorten_url;
            $url->save();
        }
        return $url;
    }
}
