<?php

namespace App\Jobs;

use App\Models\Url;
use App\Service\UrlService;
use Exception;
use Illuminate\Support\Facades\Log;

class UrlShortJob extends BaseJob
{
    public Url $url;

    public function __construct(Url $url)
    {
        parent::__construct();
        $this->url = $url;
    }
    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        parent::handle();
        $url_metadata = UrlService::get_url_metadata($this->url->url);
        $this->url->title = $url_metadata['title'];
        $this->url->save();
    }

    /**
     * Handle failed job.
     *
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Log::error('Event failed');
        Log::error($exception->getMessage());
        Log::error($exception->getFile());
        Log::error("Downloading $this->q");
        $this->exception = $exception->getMessage();
    }
}
