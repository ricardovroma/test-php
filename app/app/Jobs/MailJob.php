<?php

namespace App\Jobs;

use Exception;
use Illuminate\Support\Facades\Mail;

class MailJob extends BaseJob
{
    public $email;
    public $mail;

    public function __construct($mail, $email)
    {
        parent::__construct();
        $this->email = $email;
        $this->mail = $mail;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        parent::handle();
        $_return = Mail::to($this->email)->send($this->mail);
        print(sprintf("%s Enviado para o email: %s \n", $_return, $this->email));
    }
}
