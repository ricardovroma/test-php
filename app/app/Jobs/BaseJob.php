<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BaseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $backoff = 30;

    public $exception;

    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        print_r('Event handled');
        print(sprintf("Attempts: %s \n", $this->attempts()));
    }

    /**
     * Handle failed job.
     *
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        print_r('Event failed');
        print_r($exception->getMessage());
        print_r($exception->getFile());
        $this->exception = $exception->getMessage();
    }
}
