<?php

namespace App\Exceptions;

use Exception;

class REImportValidationException extends Exception
{
    /**
     * @var array
     */
    private $data;

    /**
     * ActuationImportValidationException constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct();
        $this->data = $data;
    }

    public function getData() {
        return $this->data;
    }
}
