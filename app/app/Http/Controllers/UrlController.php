<?php
namespace App\Http\Controllers;
use App\Http\Resources\UrlResource;
use App\Jobs\UrlShortJob;
use App\Models\Url;
use App\Service\UrlService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UrlController extends Controller {

    public function shorten(Request $request) {
        $validator = Validator::make($request->all(), [
            'url' => 'required|url',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $url = UrlService::save($request->url);
        return new UrlResource($url);
    }

    public function home(Request $request)
    {
        $errors = null;
        $url_resource = null;

        if($request->method() == "POST") {
            $validator = Validator::make($request->all(), [
                'url' => 'required|url',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
            } else {
                $url_resource = $this->shorten($request)->toArray($request);
            }
        }


        $urls = Url::orderBy('accessed', 'desc')->limit(100)->get();
        return view('home', [
            'data' => [
                'urls' => $urls,
                "errors" => $errors,
                "url_resource" => $url_resource,
                'base_url' => env('APP_URL', false)."/api",
            ]
        ]);
    }

    public function see(Request $request, string $shorten_url): \Illuminate\Http\RedirectResponse
    {
        $url = UrlService::get_shorten_url($shorten_url);
        return redirect()->away($url->url);
    }

    public function top100(Request $request): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $pageSize = 100;
        $data = Url::orderBy('accessed', 'desc');
        return UrlResource::collection(
            $data->paginate($pageSize)
        );
    }

}
