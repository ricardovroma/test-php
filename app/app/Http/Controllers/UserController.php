<?php
namespace App\Http\Controllers;
use App\Http\Resources\BusinessRuleResource;
use App\Jobs\UrlShortJob;

use App\Rules\NewPasswordRule;
use App\Utils\MyValidator;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Resources\BooleanResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {

    public function hello(Request $request) {
        return response()->json("hello", 200);
    }

    public function apply_request_filters($items, Request $request) {

        if($request->query("name")) {
            $items = $items->where("name", "like", "%".$request->query("name")."%");
        }
        if($request->query("active")) {
            $items = $items->where("active", $request->query("active"));
        }

        return $items;
    }

    public function login(Request $request) {

        $user = User::where('email', $request->get('email'))->first();
        if($user and Hash::check($request->get('password'), $user->password)) {
            if(!$user->active) {
                return response()->json(['success'=> false, "message"=> "Usuário inativo"], 401);
            } else {
                $success['token'] =  $user->createToken('MyApp', ['api'])->accessToken;
                $success['user'] = $user;
                $success['permissions'] = ($user->profile) ? User::getPermissions($user->profile) : [];
                return response()->json(['data' => $success], 200);
            }
        } else {
            return response()->json(['success'=> false, "message"=> "Usuário ou senha inválidos"], 400);
        }
    }

    public function change_password(Request $request) {
        try {
            $item = $request->user();
            $item->password = bcrypt($request->password);
            $item->save();
            return new BooleanResource(['success'=> true, "message"=> "Senha alterada com sucesso"]);
        }catch (Exception $e) {
            return new BooleanResource(['success'=> false, "message"=> "Erro ao alterar a senha", "error" => $e->getMessage()]);
        }
    }

    public function logout(Request $request) {
        try {
            $user = Auth::user()->token();
            $user->revoke();
        } catch (Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    public function show($id)
    {
        $item = User::findOrFail($id);
        return new UserResource($item);
    }

    public function store(Request $request)
    {
        $validator = $this->myValidator($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);   
        }

        $item = User::create([
            "email" => $request->email,
            "password" => bcrypt($request->password),
            "name" => $request->name,
            "active" => $request->active,
        ]);
        return new UserResource($item);
    }
	
    public function index(Request $request)
    {
        $pageSize = ($request->get('per_page')) ? $request->get('per_page') : 25;

        $items = User::orderBy("name", "asc");
        $items = $this->apply_request_filters($items, $request);
        $_items = UserResource::collection(
            $items->paginate($pageSize)
        );

        return $_items;
    }

    public function update(Request $request, $id)
    {
        $item = User::findOrFail($id);

        $validator = $this->myValidator($request, $id);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);   
        }

        $item->name = $request->name;
        $item->email = $request->email;
        if($request->password) {
            $item->password = bcrypt($request->password);
        }
        $item->active = $request->active;
        $item->save();

        return new UserResource($item);
    }

    public function myValidator($request, $id=null)
    {
        $fields = [ 
            'name' => 'required', 
            'email' => "required|email|unique:users,email,$id", 
            'active' => 'required'
        ];
        if(!$id) {
            $fields['password'] = 'required';
            $fields['c_password'] = 'required|same:password';
        }
        return MyValidator::myMake($request->all(), $fields, User::class);
    }

    public function destroy($id)
    {
        try {
            $item = User::findOrFail($id);
            $item->delete();
            
        } catch (Exception $e) {
            return response()->json(new BooleanResource(['success'=> false, "message"=> "Erro ao deletar", "error"=> $e->getMessage()]), 500);
        }
        
        return new BooleanResource(['success'=> true, "message"=> ""]);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function forgotPassword(Request $request)
    {
        $fields = ['email' => ["required", "email", new NewPasswordRule]];

        $validator = MyValidator::myMake($request->all(), $fields, User::class);

        if ($validator->fails())
        {
            return response()->json($validator->errors(), 422);
        }

        DB::beginTransaction();
        try
        {
            $user = User::where('email', $request->email)->first();
            $newPassword = "2022" . rand(100, 999);
            User::sendNewPasswordEmail($user->email, $newPassword);
            $user->password = bcrypt($newPassword);
            $user->save();
        } catch (Exception $exception)
        {
            DB::rollBack();
            return response()->json(
                new BusinessRuleResource(
                    [
                        'reason' => BusinessRuleResource::ERROR,
                        'message' => $exception->getMessage(),
                    ]
                ), 400
            );
        }
        DB::commit();

        return response()->noContent(200);
    }
}
