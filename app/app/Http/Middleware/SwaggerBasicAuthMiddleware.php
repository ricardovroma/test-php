<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SwaggerBasicAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authenticationHasPassed = false;

        if ($request->header('PHP_AUTH_USER', null) && $request->header('PHP_AUTH_PW', null))
        {
            $email = $request->header('PHP_AUTH_USER');
            $password = $request->header('PHP_AUTH_PW');

            if (Auth::attempt(['email' => $email, 'password' => $password, 'active' => true]))
            {
                $user = Auth::user();

                if ($user->profile != 'MANAGER')
                {
                    return response()->make('Usuário não é administrador.', 401, ['WWW-Authenticate' => 'Basic']);
                }

                $authenticationHasPassed = true;
            }
        }

        if ($authenticationHasPassed === false)
        {
            return response()->make('Email ou senha incorretos.', 401, ['WWW-Authenticate' => 'Basic']);
        }

        return $next($request);
    }
}
