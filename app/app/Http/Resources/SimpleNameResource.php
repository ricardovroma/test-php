<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SimpleNameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        
        if(property_exists($this, 'id')) {
            return [
              'id'   => ($this->id) ? $this->id : $this->name,
              'name' => $this->name
            ];
        } else {
            return [
                'id'   => ($this['id']) ? $this['id'] : $this['name'],
                'name' => $this['name']
              ];
        } 
        
        
    }
}
