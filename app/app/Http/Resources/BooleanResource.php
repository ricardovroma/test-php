<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BooleanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'success' => $this->resource['success'],
            "message" => $this->resource['message'],
            "error" => (array_key_exists("error", $this->resource)) ? $this->resource['error'] : "",
        ];
        
    }
}
