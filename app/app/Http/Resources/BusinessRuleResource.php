<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessRuleResource extends JsonResource
{
    // PROTOCOL
    const PROTOCOL_STATUS_FIELD_IS_MISSING = 'PROTOCOL_STATUS_FIELD_IS_MISSING';
    const PROTOCOL_SAVING_ERROR = 'PROTOCOL_SAVING_ERROR';
    const PROTOCOL_CODE_NOT_FOUND = 'PROTOCOL_CODE_NOT_FOUND';
    const SHEET_NOT_FOUND = 'SHEET_NOT_FOUND';
    const SHEET_ERROR = 'SHEET_ERROR';
    const SHEET_FORMAT_ERROR = 'SHEET_FORMAT_ERROR';
    const HOLDING_NOT_FOUND = 'HOLDING_NOT_FOUND';
    const HOLDING_IMPORT_ERROR = 'HOLDING_IMPORT_ERROR';
    const MASSIVE_LOAD_IMPORT_ERROR = 'MASSIVE_LOAD_IMPORT_ERROR';
    const ALREADY_HAS_SHARING_WITH_THE_HOLDER = "ALREADY_HAS_SHARING_WITH_THE_HOLDER";

    // GENERIC
    const ERROR = 'ERROR';
    const OC_ASSOCIATED_WITH_OTHER_RE = 'OC_ASSOCIATED_WITH_OTHER_RE';
    const THERE_IS_ALREADY_AN_ASSOCIATED_PROTOCOL = 'THERE_IS_ALREADY_AN_ASSOCIATED_PROTOCOL';
    const NOT_FOUND_IN_SPAZIO = 'NOT_FOUND_IN_SPAZIO';
    const NOT_FOUND_IN_PLOP_TX = 'NOT_FOUND_IN_PLOP_TX';
    const FOUND_IN_PLOP_ACCESS_WITH_STATUS_ACTIVATED_OR_READY = 'FOUND_IN_PLOP_ACCESS_WITH_STATUS_ACTIVATED_OR_READY';
    const COMPLEX_ORDER_COMBINATION_AND_SITE_ID_NOT_FOUND_IN_PLOP_ACCESS = 'COMPLEX_ORDER_COMBINATION_AND_SITE_ID_NOT_FOUND_IN_PLOP_ACCESS';
    const ERROR_WITH_EXCEL_FORMAT_SENT = 'ERROR_WITH_EXCEL_FORMAT_SENT';
    const VALIDATION_ERROR = "VALIDATION_ERROR";
    const FILE_REMOVE_ERROR = 'FILE_REMOVE_ERROR';
    const AEV_VALIDATION_ERROR = 'AEV_VALIDATION_ERROR';
    const AEV_APPORTIONMENT_OF_RENT_WITH_ZERO = 'AEV_APPORTIONMENT_OF_RENT_WITH_ZERO';
    const AEV_NEED_MANAGER = 'AEV_NEED_MANAGER';
    const NEED_MANAGER = 'NEED_MANAGER';
    const AEV_EXCESS_GT_AEV_REQUESTED_FINAL = 'AEV_EXCESS_GT_AEV_REQUESTED_FINAL';
    const DELETE_DEPENDENCY = 'DELETE_DEPENDENCY';
    const AEV_REQUESTED_SIGN  = 'AEV_REQUESTED_SIGN';

    // RE
    const CHECKLIST_UPDATE_ERROR  = 'CHECKLIST_UPDATE_ERROR';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'reason' => $this->resource['reason'],
            "message" => $this->resource['message'],
            'data' => isset($this->resource['data']) ? $this->resource['data'] : null,
        ];
        
    }
}
