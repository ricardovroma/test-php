<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InicialUserSeeder extends Seeder
{

    public function run() {

        DB::table('users')->insert([
            'name' => "admin",
            'email' => 'admin@localhost.com',
            'password' => bcrypt('123'),
            'active' => 1,
        ]);
    }
}
