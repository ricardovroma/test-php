<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\UrlController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('hello', [UserController::class, 'hello'])->name('hello');

Route::post('forgot_password/', [UserController::class, 'forgotPassword']);
Route::post('login', [UserController::class, 'login'])->name('login');

Route::post('shorten', [UrlController::class, 'shorten']);
Route::get('top100', [UrlController::class, 'top100']);

Route::group(['middleware' => ['auth:api'] ], function() {

    Route::get('logout', [UserController::class, 'logout']);
    Route::put('user/change_password', [UserController::class, 'change_password']);

	Route::apiResource('user', UserController::class);

});
