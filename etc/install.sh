#!/usr/bin/env bash

docker exec -i app sh -c 'cd app/; chmod +x build.sh; ./build.sh'
docker exec -i app sh -c 'cd app/; php artisan passport:install --force'
docker exec -i app sh -c 'cd app/; php artisan passport:client --personal --name=app'
docker exec -i app sh -c 'cd app/; php artisan key:generate --force'

docker exec -i app sh -c 'cd app/; php artisan db:seed --class=InicialUserSeeder'
