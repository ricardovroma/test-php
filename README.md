# README #

### Requirements ###

* Docker
* docker-compose

### Install ###
* copy app/.env.example to app/.env
* ./run_app.sh
* (cd etc; ./install.sh)
* ./build_app.sh

### Postman ###
* postman/